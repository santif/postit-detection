data = {
    'INPUT': [
        'hello-single.jpg',
        'helloworld-double.jpg',
        'holamundo-crooked.jpg',
        'holamundo-together.jpg',
        'helloworld-crooked.jpg',
        'helloworld-together.jpg',
        'holamundo-double.jpg',
        'hola-single.jpg'
    ],
    'INPUT_PATH': 'input/muestras_tesina/',
    'OUTPUT_PATH': 'output/muestras_tesina/'
}

contour = {
    'STROKE_WIDTH': 10,
    'COLOR': (0, 255, 0),
    'PADDING': 0
}

approx_poly = {
    'STROKE_WIDTH': 10,
    'COLOR': (255, 0, 0)
}

threshold = {
    'EROSION_DELTA': 10,
    'DILATATION_DELTA': 40
}

blob = {
    'MIN_W': 250,
    'MIN_H': 250
}

text = {
    'SEPARATOR': ' / '
}
