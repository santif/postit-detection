import tinify
from os import listdir
from settings import data

tinify.key = "gTdXnWFiCrowQOY7EA3JuvGJFJ70weND"

for file in listdir(data["OUTPUT_PATH"]):
    path = data["OUTPUT_PATH"] + file
    print("Optimizing " + path + "...")
    tinify.from_file(path).to_file(path.replace(".jpg", ".tinified.jpg"))