import argparse
import ntpath
import logging
import numpy as np
from pipeline import threshold, contours
from pipeline.segment import segment
from pipeline import ocr
from skimage import io
from skimage.color import gray2rgb
from utils.bool_img_to_int import bool_img_to_array

logging.basicConfig(filename='logs/out.log', level=logging.INFO)


def show_image(img, debug, method='Showing image'):
    if debug:
        print(method)
        io.imshow(img)
        io.show()


def detect(path, debug=False):
    img = io.imread(path)
    times = []

    show_image(img, debug, 'Original')

    thresh, thresh_bench = threshold.apply(img)
    times.append(thresh_bench)

    postprocessed, post_bench = threshold.de_noise(thresh)
    show_image(postprocessed, debug, 'De-noised')
    times.append(post_bench)

    cnts, cnts_bench = contours.find(postprocessed)
    times.append(cnts_bench)

    approx_cnts, approx_bench = contours.approximate(cnts)
    times.append(approx_bench)

    (detections, cnt_img), segment_bench = segment(img, approx_cnts)
    times.append(segment_bench)

    show_image(cnt_img, debug, 'Contour')

    for idx, detection in enumerate(detections):
        ocr_preprocessed = ocr.preprocess(detection["img"])
        show_image(ocr_preprocessed, debug, 'OCR Preprocessing')

        detection["text"] = ocr.run(ocr_preprocessed)

        if debug:
            print("File: {}, Detection: {}, OCR result: {}".format(path, str(idx), detection["text"]))

    return detections, np.hstack((
        img,
        gray2rgb(bool_img_to_array(thresh)),
        gray2rgb(bool_img_to_array(postprocessed)),
        cnt_img
    )), times


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-d", "--debug", help="Enable debug mode", action="store_true")
    ap.add_argument("-i", "--image", required=True, help="Path to the image")

    args = ap.parse_args()
    filename = ntpath.basename(args.image)

    detect(args.image, args.debug)
