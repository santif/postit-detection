import numpy as np
from lib.Sample import Sample

np.set_printoptions(threshold=np.nan)

features = [
    (1, 2, 3),
    (0, 1),
    (0, 1),
    (0, 1),
    (0, 1),
    (0, 1),
    (0, 1),
    (0, 1)
]

combinations = np.array(np.meshgrid(*features)).T.reshape(-1, len(features))

# Sort the feature matrix according to the weights given to each attribute
sorted_matrix = sorted(combinations.tolist(), key=Sample.score)

mapped_matrix = map(Sample.encode_features, sorted_matrix)

np.savetxt('feature_permutations.v3.txt', mapped_matrix, fmt='%s.jpg')
