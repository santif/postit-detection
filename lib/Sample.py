import numpy as np


class Sample:
    """
    Represents a postit detection sample.
    A sample can represent its features in two possible ways:
        * Encoded: as sequence of integers representing either the existence or not of an attribute (boolean) or an index for a mapping to an enumerable.
        * Decoded: a string containing the attributes in a human-readable format, delimited by a hyphen (-).

    A sample will always store it's features in encoded format, and can
    represent them in decoded format if needed.

        Attributes:
            feature_meta   List containing dictionaries describing each feature metadata.
            mappings       List of mappings for each feature nominal values
    """

    DELIMITER = "-"

    feature_meta = [
        {
            'id': 'n_postits',
            'name': '# Postit',
            'type': int,
            'mappings': ['0', '1', '2', '3', '4'],
            'weight': 1
        },
        {
            'id': 'font_color',
            'name': 'Font color',
            'type': str,
            'mappings': ['black', 'blue'],
            'weight': 2
        },
        {
            'id': 'has_bkgd',
            'name': 'Has Background',
            'type': bool,
            'mappings': ['no_background', 'background'],
            'weight': 8
        },
        {
            'id': 'lang',
            'name': 'Lang',
            'type': str,
            'mappings': ['en', 'es'],
            'weight': 5
        },
        {
            'id': 'skewed',
            'name': 'Skewed',
            'type': bool,
            'mappings': ['deskewed', 'skewed'],
            'weight': 8

        },
        {
            'id': 'postit_color_difficulty',
            'name': 'Color lvl',
            'type': str,
            'mappings': ['easy', 'hard'],
            'weight': 8
        },
        {
            'id': 'casing',
            'name': 'Casing',
            'type': str,
            'mappings': ['cap', 'min'],
            'weight': 5
        },
        {
            'id': 'join',
            'name': 'Join',
            'type': bool,
            'mappings': ['separated', 'join'],
            'weight': 21
        },
        {
            'id': 'text',
            'name': 'Text',
            'type': str,
            'mappings': [],
            'weight': 21
        }
    ]

    mappings = map(lambda f: f['mappings'], feature_meta)

    def __init__(self, features):
        """Creates a sample object with encoded features"""
        self.features = features

    @classmethod
    def get_attributes(cls, attr):
        return map(lambda f: f[attr], cls.feature_meta)

    @classmethod
    def fromfilename(cls, filename):
        """
        Instantiates a sample from a filename
        :param filename:str
        :return :Sample
        """
        features = filename.split(".")[0].split(cls.DELIMITER)
        return cls(map(lambda (i, _): cls.mappings[i].index(features[i]), enumerate(features)))

    @classmethod
    def score(cls, encoded_features):
        """
        The sample score is the result of applying weights to
        all features and summarizing them.
        :return:int
        """
        return sum(np.array(encoded_features) * cls.get_attributes('weight'))

    @classmethod
    def encode_features(cls, features):
        features = map(cls.feature_encoder(features), enumerate(cls.mappings))
        return cls.DELIMITER.join(features)

    @classmethod
    def feature_encoder(cls, features):
        """
        A lambda function that decoded features
        :param features:
        :return:
        """
        return lambda (i, feature): cls.mappings[i][features[i]]

    @classmethod
    def extract_features(cls, filename):
        return filename.split(".")[0].split(cls.DELIMITER)

    def decode_features(self):
        """
        Returns sample features in decoded format.
        :return :list
        """
        return map(
            lambda (i, _): self.mappings[i][self.features[i]],
            enumerate(self.mappings)
        )

    def decoded_features_as_filename(self):
        """
        Returns features in a format suitable for filename.
        :return :str
        """
        return self.DELIMITER.join(self.decode_features())
