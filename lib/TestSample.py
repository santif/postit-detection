import unittest
from lib.Sample import Sample


class TestSample(unittest.TestCase):

    def setUp(self):
        self.encoded_features = [1, 0, 0, 0, 0, 0, 0, 0]

    def test_decode_features(self):
        sample = Sample(self.encoded_features)
        self.assertIsInstance(sample.decode_features(), list)

    def test_decoded_filename(self):
        sample = Sample(self.encoded_features)
        filename = sample.decoded_features_as_filename()
        self.assertIsInstance(filename, str)
        self.assertEquals(filename, '1-black-no_background-en-deskewed-easy-cap-separated')

    def test_encode_features(self):
        pass

    def test_extract_features(self):
        filename = '1-black-no_background-en-deskewed-easy-cap-separated'
        features = Sample.extract_features(filename)
        self.assertIsInstance(features, list)
        self.assertEquals(features, filename.split("-"))

    def test_fromfilename(self):
        sample = Sample.fromfilename('1-black-no_background-en-deskewed-easy-cap-separated.jpg')
        self.assertEquals(sample.features, self.encoded_features)

    def test_score(self):
        score = Sample.score(self.encoded_features)
        self.assertTrue(isinstance(score, int))
        self.assertEquals(score, 1)


if __name__ == '__main__':
    unittest.main()
