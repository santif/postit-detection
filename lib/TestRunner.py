import settings
from detect import detect
import logging
from skimage import io
from Sample import Sample
import numpy as np


class TestRunner:
    detections = []
    samples = []
    times = []
    images = []
    max_samples = 5
    expected_samples = []

    def __init__(self, samples, expected_samples):
        self.samples = samples
        self.expected_samples = expected_samples

    def get_samples(self):
        return self.samples[:self.max_samples]

    def run(self, debug=False, write=True, max_samples=10):
        self.max_samples = max_samples

        for idx, filename in enumerate(self.samples[:max_samples]):
            self.process_sample(filename, write, debug)

        return self.detections, self.times

    def process_sample(self, filename, write, debug):
        complete_path = settings.data["INPUT_PATH"] + filename
        detections, img, times = detect(complete_path, debug)
        self.detections.append(detections)
        self.images.append(img)
        self.times.append(times)
        if debug:
            logging.info('Writing sample output to ' + settings.data["OUTPUT_PATH"] + filename + ".out")
        if write:
            io.imsave(settings.data["OUTPUT_PATH"] + filename.replace(".jpg", ".out.jpg"), img)
        if debug or write:
            for j, detection in enumerate(detections):
                detection_filename = filename.replace(".jpg", "-" + str(j + 1) + ".out.jpg")

                if write:
                    io.imsave(settings.data["OUTPUT_PATH"] + detection_filename, detection["img"])

                if debug:
                    logging.info('Writing sample result output to ' + detection_filename)

    def effectiveness(self):
        """
        Calculates effectiveness for all the data-set based on total detections and
        expected detections.
        :return:list
        """
        eff = []
        for total, expected in zip(self.total_detections(), self.expected_detections()):
            if expected > total:
                total = total - (total - expected)
            eff.append(expected * 100 / total)
        return eff

    def total_detections(self):
        return map(len, self.detections[:self.max_samples])

    def features(self):
        return map(Sample.extract_features, self.expected_samples[:self.max_samples])

    def expected_detections(self):
        return [int(item[0]) for item in self.features()]

    def feature_names(self):
        return ['Filename'] + Sample.get_attributes('name') + ['# Detections', 'Accuracy (%)']

    def total_exec_time(self):
        return sum(map(float, np.array(self.times)[:, :, 2].flatten()))

    def mean_effectiveness(self):
        return np.mean(self.effectiveness())

    def text_results(self):
        return [settings.text["SEPARATOR"].join(map(lambda d: d["text"], detection)) for detection in self.detections]
