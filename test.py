from os import listdir
from settings import data
from terminaltables import AsciiTable
import numpy as np
from lib.Sample import Sample
from lib.TestRunner import TestRunner
import argparse

"""
This test is run under the samples generated and written to 
the feature_permutations.v1.txt file.
"""


def result_tables(detector):
    effectiveness = detector.effectiveness()
    total_detections = detector.total_detections()
    features = detector.features()
    samples = detector.get_samples()
    text_detections = detector.text_results()

    result_table = np.column_stack(
        (
            samples,
            features,
            text_detections,
            total_detections,
            effectiveness
        )
    ).tolist()

    result_table.insert(0, detector.feature_names())

    # Build general information table
    basic_info_table_data = [
        ['# samples', 'Avg effectiveness', 'Load time (seconds)'],
        [N_SAMPLES, detector.mean_effectiveness(), detector.total_exec_time()]
    ]

    return AsciiTable(basic_info_table_data), AsciiTable(result_table)


def main():
    expected_sample_names = list(open("./feature_permutations.v1.txt"))

    samples = sorted(listdir(data["INPUT_PATH"]), key=lambda x: int(x.split(".")[0]))

    runner = TestRunner(samples, expected_sample_names)

    runner.run(args.debug, True, N_SAMPLES)

    basic_info_table, result_table = result_tables(runner)

    print(basic_info_table.table)
    print(result_table.table)


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-n", "--samples",
                    help="Number of samples to test, starting from sample 1",
                    default=10)
    ap.add_argument("-d", "--debug", help="Enable debug mode", action="store_true")
    ap.add_argument("-b", "--benchmark", help="Enable benchmarking mode to analyze pipeline performance",
                    action="store_false")
    args = ap.parse_args()
    N_SAMPLES = int(args.samples)
    main()
else:
    N_SAMPLES = 10
