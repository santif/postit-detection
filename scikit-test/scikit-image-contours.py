import matplotlib.pyplot as plt
import pdb
from skimage import measure, io

pdb.set_trace()

img = io.imread("input/muestras_tesina/1.jpg", as_gray=True)
contours = measure.find_contours(img, 0.8)

fig, ax = plt.subplots()
ax.imshow(img, interpolation='nearest', cmap=plt.cm.gray)

for n, contour in enumerate(contours):
    ax.plot(contour[:, 1], contour[:, 0], linewidth=2)

ax.axis('image')
ax.set_xticks([])
ax.set_yticks([])

plt.show()