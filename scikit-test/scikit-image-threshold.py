from os import walk

import pipeline.hsv_filter as hsv_filter
import matplotlib.pyplot as plt
from skimage import io
from skimage.color import rgb2gray
from skimage.filters import threshold_triangle as threshold
from skimage.morphology import binary_erosion, binary_dilation
from skimage.morphology import disk

for _, _, files in walk("input/muestras_tesina"):
    for filename in files:
        print(filename)

        image = io.imread("input/muestras_tesina/" + filename)
        plt.imshow(image, cmap=plt.cm.gray)
        plt.show()

        hsv_filtered = hsv_filter.apply(image)
        plt.imshow(hsv_filtered, cmap=plt.cm.gray)
        plt.show()

        image_gray = rgb2gray(hsv_filtered)
        thresh = threshold(image_gray)

        binary_min = image_gray > thresh

        plt.imshow(binary_min, cmap=plt.cm.gray)
        plt.show()

        # Morphological transformations
        opened = binary_erosion(binary_min, selem=disk(2))
        eroded = binary_dilation(opened, selem=disk(4))
        print 'Thresholding finished.'

        plt.imshow(eroded, cmap=plt.cm.gray)
        plt.show()