from skimage import io
from skimage.filters import try_all_threshold
import matplotlib.pyplot as plt

img = io.imread("input/muestras_tesina/6.jpg", as_gray=True)

fig, ax = try_all_threshold(img, figsize=(10, 8), verbose=False)
plt.show()
