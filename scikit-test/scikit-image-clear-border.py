import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from skimage import io
from skimage.color import rgb2gray, label2rgb
from skimage.filters import threshold_triangle as threshold
from skimage.morphology import binary_erosion, binary_dilation
from skimage.morphology import disk
from skimage.segmentation import clear_border
from skimage.measure import label, regionprops

image = io.imread("input/muestras_tesina/1.jpg")
image_gray = rgb2gray(image)

thresh = threshold(image_gray)

binary_min = image_gray > thresh
plt.imshow(binary_min, cmap=plt.cm.gray)
plt.show()

opened = binary_erosion(binary_min, selem=disk(2))
plt.imshow(opened, cmap=plt.cm.gray)
plt.show()

eroded = binary_dilation(opened, selem=disk(4))
plt.imshow(eroded, cmap=plt.cm.gray)
plt.show()

cleared = clear_border(eroded)

label_image = label(cleared)
image_label_overlay = label2rgb(label_image, image=image)

fig, ax = plt.subplots(figsize=(10, 6))
ax.imshow(image_label_overlay)
for region in regionprops(label_image):
    # take regions with large enough areas
    if region.area >= 100:
        # draw rectangle around segmented coins
        minr, minc, maxr, maxc = region.bbox
        rect = mpatches.Rectangle((minc, minr), maxc - minc, maxr - minr,
                                  fill=False, edgecolor='red', linewidth=2)
        ax.add_patch(rect)

ax.set_axis_off()
plt.tight_layout()
plt.show()
