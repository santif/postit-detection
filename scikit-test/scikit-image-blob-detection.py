import matplotlib.pyplot as plt
import pdb
from math import sqrt
from skimage import io
import numpy as np
from skimage.color import rgb2gray, gray2rgb
from skimage.feature import blob_log, blob_dog, blob_doh
from skimage.filters import threshold_triangle as threshold
from skimage.morphology import binary_erosion, binary_dilation
from skimage.filters.rank import median
from skimage.morphology import disk

image = io.imread("input/muestras_tesina/3.jpg")
image_gray = rgb2gray(image)

thresh = threshold(image_gray)

binary_min = image_gray > thresh
plt.imshow(binary_min, cmap=plt.cm.gray)
plt.show()

opened = binary_erosion(binary_min, selem=disk(2))
plt.imshow(opened, cmap=plt.cm.gray)
plt.show()

eroded = binary_dilation(opened, selem=disk(4))
plt.imshow(eroded, cmap=plt.cm.gray)
plt.show()
pdb.set_trace()

blobs_log = blob_log(eroded, max_sigma=30, num_sigma=10, threshold=.1)

blobs_log[:, 2] = blobs_log[:, 2] * sqrt(2)
blobs_dog = blob_dog(eroded, max_sigma=30, threshold=.1)
blobs_dog[:, 2] = blobs_dog[:, 2] * sqrt(2)

blobs_doh = blob_doh(eroded, max_sigma=30, threshold=.01)

blobs_list = [blobs_log, blobs_dog, blobs_doh]
colors = ['yellow', 'lime', 'red']
titles = ['Laplacian of Gaussian', 'Difference of Gaussian',
          'Determinant of Hessian']
sequence = zip(blobs_list, colors, titles)

fig, axes = plt.subplots(1, 3, figsize=(9, 3), sharex=True, sharey=True)
ax = axes.ravel()

for idx, (blobs, color, title) in enumerate(sequence):
    ax[idx].set_title(title)
    ax[idx].imshow(eroded, interpolation='nearest')
    for blob in blobs:
        y, x, r = blob
        c = plt.Circle((x, y), r, color=color, linewidth=2, fill=False)
        ax[idx].add_patch(c)
    ax[idx].set_axis_off()

plt.tight_layout()
plt.show()