import cv2
import numpy as np
import legacy.legacy_pipeline.threshold as threshold
from legacy.legacy_pipeline import contours
from legacy.legacy_pipeline.segment import segment
from legacy.legacy_pipeline.peak_detection import peak_detection
from legacy.legacy_pipeline.ocr import ocr
import argparse
import ntpath
import logging

logging.basicConfig(filename='logs/out.log', level=logging.INFO)


def show_image(name, img, debug, method='Showing image'):
    if debug:
        logging.info(filename + ' ' + method)
        cv2.resizeWindow(name, 600, 600)
        cv2.imshow(name, img)
        cv2.waitKey(0)


def detect(filename, debug=False):
    logging.info('- Processing image' + filename)

    img = cv2.imread(filename)
    if debug:
        cv2.namedWindow(filename, cv2.WINDOW_NORMAL)

    # Show original image
    show_image(filename, img, debug, 'Original')

    # Calculate treshold values for each channel
    ranges = peak_detection(img)

    # Apply threhsolding to isolate postits from background
    thresh = threshold.apply(img, ranges)
    show_image(filename, thresh, debug, 'Threshold')

    # Contour preprocessing
    postprocessed = threshold.postprocess(thresh)
    show_image(filename, postprocessed, debug, 'Threshold Postprocess')

    # Get the contours for the squares detected in the image
    cnt_img, cnts = contours(img, postprocessed)
    show_image(filename, cnt_img, debug, 'Contours')

    # Separate detected contours from original image
    detections = segment(img, cnts, padding=-10)

    result = np.hstack((
        img, thresh, postprocessed, cnt_img
    ))

    show_image(filename, result, debug, 'Final')

    # Process each detection separately
    for idx, detection in enumerate(detections):
        # binarisation()
        # denoising()
        # deskewing()
        # show_image(filename, detection['img'])

        if debug:
            detection_image = detection["img"]
            print("OCR Detection: " + str(ocr(detection_image)))
            show_image(filename, detection_image, True)

    return detections, result


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-d", "--debug", help="Enable debug mode", action="store_true")
    ap.add_argument("-i", "--image", required=True, help="Path to the image")

    args = ap.parse_args()
    filename = ntpath.basename(args.image)

    detect(args.image, args.debug)
