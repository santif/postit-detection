import cv2
import matplotlib.pyplot as plt


def rgb_hist(img):
    hists = []
    color = ('b', 'g', 'r')
    for i, col in enumerate(color):
        hist = cv2.calcHist([img], [i], None, [256], [0, 256])
        hists.append(hist)
    return hists


def plot_rgb_hist(hists):
    color = ('b', 'g', 'r')
    for i, col in enumerate(color):
        plt.plot(hists[i], color=col)
        plt.xlim([0, 256])
    plt.show()