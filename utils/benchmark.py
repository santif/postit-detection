import time
import functools


def benchmark(func):
    """
    Profile decorator
    :param func:The function to be benchmarked
    :return:
    """

    @functools.wraps(func)
    def benchmark_function(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        runtime = end - start

        return result, [func.__module__, func.__name__, runtime]

    return benchmark_function
