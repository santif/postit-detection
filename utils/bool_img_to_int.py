import numpy as np


def bool_img_to_array(img):
    return img.astype(np.uint8) * (np.ones(img.shape).astype(np.uint8) * 255)
