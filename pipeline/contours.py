from skimage import measure, draw, morphology
import numpy as np
import settings
import random
from utils.benchmark import benchmark


@benchmark
def find(postprocessed):
    cnts = measure.find_contours(postprocessed, 0.2)

    return cnts


@benchmark
def approximate(cnts):
    approx_cnts = []
    for cnt in cnts:
        approx_cnt = measure.approximate_polygon(cnt, tolerance=100)
        if len(approx_cnt) >= 4:
            approx_cnts.append(approx_cnt)
    return approx_cnts
