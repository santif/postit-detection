from skimage.morphology import binary_erosion, binary_dilation, disk
import hsv_filter
from utils.benchmark import benchmark


@benchmark
def apply(img):
    """
    Applies Triangle Threshold algorithm to an RGB image.
    :param img: A numpy array representing the image to apply threshold to.
    :return: A binary version of the image with threshold applied.
    """

    hsv_filtered = hsv_filter.apply(img)

    return hsv_filtered


@benchmark
def de_noise(img):
    """
    Applies denoising techniques to *img*
    :param img: A binary image (*img*) to apply the techniques to.
    :return: A numpy array representing the de-noised image.
    """
    eroded = binary_erosion(img, selem=disk(2))

    return binary_dilation(eroded, selem=disk(4))
