import os
import numpy as np
from subprocess import Popen, PIPE
from skimage import io
from skimage.filters import threshold_isodata
from skimage.restoration import denoise_tv_chambolle
from settings import data


def preprocess(image):

    " Denoise """
    image = denoise_tv_chambolle(image, weight=0.8, multichannel=False)

    " Binarization """
    result = image > threshold_isodata(image)

    result = (result.astype(np.uint8)) * (np.ones(result.shape, dtype=np.uint8) * 255)

    return result


def run(image, lang=None, psm=None, strip=True):

    tempImage = "{}{}_temp.jpg".format(data["OUTPUT_PATH"], os.getpid())

    io.imsave(tempImage, image)

    args = ["tesseract", tempImage, "result"]
    if lang is not None:
        args.append("-l")
        args.append(lang)
    if psm is not None:
        args.append("-psm")
        args.append(str(psm))
    proc = Popen(args, stdout=PIPE, stderr=PIPE)
    ret = proc.communicate()

    code = proc.returncode
    if code != 0:
        if code == 2:
            raise Exception("File not found")
        if code == -11:
            raise Exception("Language code invalid: " + ret[1])
        else:
            raise Exception(ret[1])

    f = open("result.txt", "r")
    text = f.read()
    f.close()
    os.remove("result.txt")
    os.remove(tempImage)

    return text.strip() if strip else text