from settings import blob, contour, approx_poly
import numpy as np
from utils.benchmark import benchmark
from skimage.draw import polygon_perimeter
from skimage.morphology import binary_dilation, disk
from pipeline import color


@benchmark
def segment(img, cnts, padding=contour["PADDING"]):
    """
    Receives and image and a list of contours and segments the
    contours from the image using the bounding rect,
    :param img:img
    :param cnts:list
    :param padding:int
    :return:list
    """
    detections = []
    result = img.copy()

    for i, cnt in enumerate(cnts):
        draw_cnt(cnt, result)

        x, y, w, h = bounding_rect(cnt)

        bound_cnt = [
            [y + padding, x + w - padding],
            [y + padding, x + padding],
            [y - h + padding, x + padding],
            [y + h - padding, x + w - padding]
        ]
        draw_cnt(bound_cnt, result, color=approx_poly['COLOR'], stroke_width=approx_poly['STROKE_WIDTH'])

        if w > blob['MIN_W'] and y > blob['MIN_H']:
            detection = img[
                        int(y - padding):
                        int(y + h + padding),
                        int(x - padding):
                        int(x + w + padding)
                        ]

            detections.append({
                'x': x,
                'y': y,
                'w': w,
                'h': h,
                'img': detection,
                'color': color.detect(detection)
            })

    return detections, result


def draw_cnt(cnt, img, color=contour['COLOR'], stroke_width=contour['STROKE_WIDTH']):
    cnt = np.array(cnt)
    mask = np.zeros((img.shape[0], img.shape[1]))
    plp = polygon_perimeter(cnt[:, 0], cnt[:, 1])
    mask[plp] = 1
    mask = binary_dilation(mask, disk(stroke_width))
    img[mask] = color


def bounding_rect(cnt):
    """
    Get the bounding rect metadata (x, y, width and height) for a
    given contour
    :param cnt:contour
    :return:(x,y,w,h)
    """
    min_x = cnt[:, 1].min()
    max_x = cnt[:, 1].max()
    min_y = cnt[:, 0].min()
    max_y = cnt[:, 0].max()
    width = max_x - min_x
    height = max_y - min_y
    return min_x, min_y, width, height
