from skimage.color import rgb2hsv


def apply(img, min=0.20):
    """
    Transform the given image to HSV format and returns a minimum saturation mask.
    :img: A numpy array representing the image to apply filter to.
    :min: saturation of the image threshold. Default is 0.20.
    :return: Image mask to apply the threshold
    """

    hsv = rgb2hsv(img)
    saturation_channel = hsv[:, :, 1]
    mask = (saturation_channel > min)

    return mask
