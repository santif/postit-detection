import numpy as np


def detect(img):
    return '#%02x%02x%02x' % tuple(np.mean(img, axis=(0, 1)))
